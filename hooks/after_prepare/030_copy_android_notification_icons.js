#!/usr/bin/env node
var fs = require('fs'),
  path = require('path'),
  rootdir = process.argv[2],
  filestocopy = [{
    'resources/android/icon/drawable-hdpi-icon.png':
      'platforms/android/res/drawable-hdpi/ic_stat_onesignal_default.png'
  }, {
    'resources/android/icon/drawable-mdpi-icon.png':
      'platforms/android/res/drawable-mdpi/ic_stat_onesignal_default.png'
  }, {
    'resources/android/icon/drawable-xhdpi-icon.png':
      'platforms/android/res/drawable-xhdpi/ic_stat_onesignal_default.png'
  }, {
    'resources/android/icon/drawable-xxhdpi-icon.png':
      'platformas/android/res/drawable-xxhdpi/ic_stat_onesignal_default.png'
  }, {
    'resources/android/icon/drawable-xxxhdpi-icon.png':
      'platforms/android/res/drawable-xxxhdpi/ic_stat_onesignal_default.png'
  }];

// No need to configure below
filestocopy.forEach(function(obj) {
  Object.keys(obj).forEach(function(key) {
    var val = obj[key],
      srcfile = path.join(rootdir, key),
      destfile = path.join(rootdir, val);
    console.log('Copying ' + srcfile + ' to ' + destfile);
    var destdir = path.dirname(destfile);
    if(fs.existsSync(srcfile) && fs.existsSync(destdir)) {
      fs.createReadStream(srcfile).pipe(
        fs.createWriteStream(destfile)
      );
    }
  });
});