import { Component, ViewChild } from '@angular/core';
import * as sha1 from 'js-sha1';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'credencial',
  templateUrl: 'credencial.html'
})
export class CredencialComponent {
  @ViewChild('anverso') anverso;
  @ViewChild('informacion') info;
  width: number;
  height: number;
  credencial: object;
  QRData: string;
  random: number;

  constructor(
    private nativeStorage: NativeStorage
  ) {
    let fecha = new Date();
    let str_fecha = fecha.getDate() + '-' + fecha.getMonth() + '-' + fecha.getFullYear() + 'T' +
      fecha.getHours() + ':' + fecha.getMinutes();

    this.nativeStorage.getItem('fotografia')
      .then(fotografia => {
        this.nativeStorage.getItem('nombres')
          .then(nombres => {
            this.nativeStorage.getItem('apellidos')
              .then(apellidos => {
                this.nativeStorage.getItem('carrera')
                  .then(carrera => {
                    this.nativeStorage.getItem('run')
                      .then(run => {
                        this.credencial = {
                          foto: fotografia,
                          nombre: nombres.split(' ')[0] + ' ' + apellidos,
                          carrera: carrera,
                          run: this.formatearRUN(run)
                        };
                      }, err => {
                        console.log('Error al obtener \'run\': ', err);
                      });
                  }, err => {
                    console.log('Error al obtener \'carrera\': ', err);
                  });
              }, err => {
                console.log('Error al obtener \'apellidos\': ', err);
              });
          }, err => {
            console.log('Error al obtener \'nombres\': ', err);
          });
      }, err => {
        console.log('Error al obtener \'fotografia\': ', err);
      });


    // Generar valor aleteatorio
    this.random = Math.floor(Math.random() * (9999999999999 - 1000000000000 + 1)) + 1000000000000;

    // Generar data de código QR
    this.nativeStorage.getItem('run')
      .then(run => {
        this.QRData = 'UV' + fecha.getUTCFullYear() + this.random + ',' + run + ',' + sha1(str_fecha);
        console.log('QRData: ', this.QRData);
      }, err => {
        console.log('Error al obtener \'run\': ', err);
      });
  }

  ngAfterViewInit() {
    // console.log('Anverso-Alto: ', this.anverso.nativeElement.offsetHeight);
    // console.log('Anverso-Ancho: ', this.anverso.nativeElement.offsetWidth);


    this.height = this.anverso.nativeElement.offsetWidth;
    this.width = this.anverso.nativeElement.offsetHeight - 44;

    // console.log('Info-Alto: ', this.info.nativeElement.offsetHeight);
    // console.log('Info-Ancho: ', this.info.nativeElement.offsetWidth);
  }

  calcularDV(numero) {
    var nuevo_numero = numero.toString().split('').reverse().join('');
    for(var i = 0, j = 2, suma = 0; i < nuevo_numero.length; i++, ((j == 7) ? j = 2 : j++)) {
      suma += (parseInt(nuevo_numero.charAt(i)) * j);
    }
    var n_dv = 11 - (suma % 11);
    return ((n_dv == 11) ? 0 : ((n_dv == 10) ? 'K' : n_dv));
  }

  formatearRUN(run) {
    console.log(run);
    run = run.concat(this.calcularDV(run));
    console.log(run);
    console.log(this.calcularDV(run));
    var actual = run.replace(/^0+/, '');
    if(actual != '' && actual.length > 1) {
      var sinPuntos = actual.replace(/\./g, '');
      var actualLimpio = sinPuntos.replace(/-/g, '');
      var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
      var runPuntos = '';
      var i = 0;
      var j = 1;
      for(i = inicio.length - 1; i >= 0; i--) {
        var letra = inicio.charAt(i);
        runPuntos = letra + runPuntos;
        if(j % 3 == 0 && j <= inicio.length - 1) {
          runPuntos = '.' + runPuntos;
        }
        j++;
      }
      var dv = actualLimpio.substring(actualLimpio.length - 1);
      runPuntos = runPuntos + "-" + dv;
    }
    return runPuntos;
  }
}