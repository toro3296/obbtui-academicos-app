import { Directive, HostBinding, ElementRef } from '@angular/core';

@Directive({
  selector: '[show-hide-input]'
})
export class ShowHideInput {
  @HostBinding() type: string;

  constructor(public el: ElementRef) {
    let env = this;
    env.type = 'password';
  }

  changeType(type:string) {
    let env = this;
    env.type = type;
    env.el.nativeElement.children[0].type = env.type;
  }
}
