import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { AccesoProvider } from '../../providers/acceso/acceso';
import { UxProvider } from '../../providers/ux/ux';
import { PresentacionPage } from '../presentacion/presentacion';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { GlobalVar } from '../../config';

@Component({
  selector: 'login-page',
  templateUrl: 'acceso.html'
})
export class AccesoPage {
  accederForm: FormGroup;

  constructor(
    private nav: NavController,
    private nativeStorage: NativeStorage,
    private accesoProvider: AccesoProvider,
    private ux: UxProvider
  ) {
    this.accederForm = new FormGroup({
      run: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.maxLength(8) ]),
      contrasena: new FormControl('', Validators.required)
    });
  }

  olvidarContrasena() {
    window.open(GlobalVar.OLVIDAR_CONTRASENA, '_system');
  }

  presentacion() {
    this.nav.setRoot(PresentacionPage);
    this.nav.popToRoot();
  }

  acceder() {
    var run = this.accederForm.value.run,
      contrasena = this.accederForm.value.contrasena,
      dispositivo;

    this.nativeStorage.getItem('dispositivo')
      .then(data => {
        dispositivo = data ? data : 'chrome';

        this.ux.showCargando();
        this.accesoProvider.acceder(run, contrasena, dispositivo)
          .subscribe(dataLogin => {
            this.ux.hideCargando();
            console.log('Acceso: ', dataLogin);
            if(dataLogin.status == 'ERROR') {
              let mensaje = dataLogin.error.message.replace('Invalid Credentials', 'Usuario o contraseña incorrectos');
              this.ux.alerta(mensaje);
            } else if(dataLogin.status == 'OK') {
              let usuario = dataLogin.data;

              if(usuario.primerIngreso == 'S' || usuario.contrasenaCaducada == 'S') {
                // Enviar a portal de estudiante
                window.open(GlobalVar.PORTAL_ACADEMICO, '_system');
              } else {
                // Guardar datos personales
                this.nativeStorage.setItem('token', usuario.token);
                this.nativeStorage.setItem('run', usuario.run);
                this.nativeStorage.setItem('fotografia', usuario.fotografia);
                this.nativeStorage.setItem('movilidad', usuario.movilidad);
                this.nativeStorage.setItem('primerIngreso', usuario.primerIngreso);
                this.nativeStorage.setItem('contrasenaCaducada', usuario.contrasenaCaducada);
                this.nativeStorage.setItem('nombres', usuario.nombres);
                this.nativeStorage.setItem('apellidos', usuario.apellidos);
                this.nativeStorage.setItem('nombreCompleto', usuario.nombreCompleto);
                this.nativeStorage.setItem('email', usuario.email);
                this.nativeStorage.setItem('codPrograma', usuario.codPrograma);
                this.nativeStorage.setItem('carrera', usuario.carrera);
                this.nativeStorage.setItem('codCarrera', usuario.codCarrera);
                this.nativeStorage.setItem('numDecreto', usuario.numDecreto);
                this.nativeStorage.setItem('descEstado', usuario.descEstado);

                // Cambiar de página
                this.nav.setRoot(TabsNavigationPage);
                this.nav.popToRoot();
              }
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'dispositivo\': ', err);
      });
  }
}