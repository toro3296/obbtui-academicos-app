import { Component } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavParams, Platform } from 'ionic-angular';
import { AsignaturasProvider } from '../../providers/asignaturas/asignaturas';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-asignatura-correo',
  templateUrl: 'asignatura-correo.html',
})
export class AsignaturaCorreoPage {
  asignatura: any;
  correo: any;
  estudiantes: any;
  lista: any;

  constructor(
    private dialogs: Dialogs,
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private navParams: NavParams,
    private platform: Platform,
    private asignaturasProvider: AsignaturasProvider,
    private ux: UxProvider
  ) {
    this.asignatura = this.navParams.get('asignatura');
    this.lista = true;
    this.correo = {
      asunto: '',
      mensaje: '',
      estudiantes: []
    };

    // Obtener estudiantes
    this.getEstudiantes(this.asignatura);
  }

  ionViewDidLoad() {
    this.ga.trackView('page-asignatura-correo');
  }

  getEstudiantes(asignatura) {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.asignaturasProvider.getEstudiantes(token, asignatura)
          .subscribe(dataEstudiantes => {
            this.ux.hideCargando();
            if(dataEstudiantes.status == 'ERROR') {
              this.ux.alerta(dataEstudiantes.error.message);
            } else if(dataEstudiantes.status == 'OK') {
              this.estudiantes = dataEstudiantes.data;
              this.correo.estudiantes = this.estudiantes.map(data => data.correo);
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  sendCorreo() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.asignaturasProvider.sendCorreo(token, this.asignatura, this.correo.asunto, this.correo.mensaje, this.correo.estudiantes)
          .subscribe(dataOS => {
            this.ux.hideCargando();
            if(dataOS.status == 'ERROR') {
              this.ux.alerta(dataOS.error.message);
            } else if(dataOS.status == 'OK') {
              this.ux.alerta('Correo enviado satisfactoriamente');
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  enviarCorreo() {
    let titulo = 'Alerta';
    let botones = ['Cancelar', 'Enviar']; // Cancelar = 2, Enviar = 1

    if(this.platform.is('cordova')) {
      this.dialogs.confirm('¿Estás seguro(a) que deseas enviar este correo?', titulo, botones)
        .then(salir => {
          if(salir == 1) {
            this.sendCorreo();
          }
        })
        .catch(err => {
          console.log('Error al mostrar el diálogo: ', err);
        });
    } else {
      console.error('No se puede mostrar un mensaje de confirmación en navegador.');
      this.sendCorreo();
    }
  }
}