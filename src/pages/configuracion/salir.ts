import { Component } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavController } from 'ionic-angular';
import { AccesoPage } from '../acceso/acceso';

@Component({
  selector: 'page-salir',
  template: ''
})
export class SalirPage {

  constructor(
    private nativeStorage: NativeStorage,
    public nav: NavController
  ) {
    this.nativeStorage.clear()
      .then(() => {
        this.nativeStorage.setItem('presentacion', 'false')
          .then(() => {
            this.nav.setRoot(AccesoPage);
            this.nav.popToRoot();
          }, err => {
            console.log('Error al guardar \'presentacion\': ', err);
          });
      }, err => {
        console.log('Error al borrar sesión: ', err);
      });
  }
}
