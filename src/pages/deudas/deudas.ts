import { CallNumber } from '@ionic-native/call-number';
import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { DeudasProvider } from '../../providers/deudas/deudas';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-deudas',
  templateUrl: 'deudas.html',
})
export class DeudasPage {
  deudas: any;
  web_aranceles: string;
  numeroSeleccionado: string;
  numeros: any;

  constructor(
    private ga: GoogleAnalytics,
    private callNumber: CallNumber,
    private nativeStorage: NativeStorage,
    private deudasProvider: DeudasProvider,
    private portalAcademicoProvider: PortalAcademicoProvider,
    private ux: UxProvider
  ) {
    this.web_aranceles = 'http://aranceles.uv.cl';
    this.numeros = [
      { numero: '+56322995616', texto: '+56 32 299 5616' },
      { numero: '+56322995626', texto: '+56 32 299 5626' },
      { numero: '+56322997766', texto: '+56 32 299 7766' },
      { numero: '+56322995658', texto: '+56 32 299 5658' }
    ];

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.ux.showCargando();
    this.nativeStorage.getItem('token')
      .then(data => {
        var token = data;
        this.deudasProvider.getDeudas(token)
          .subscribe(dataDeudas => {
            this.ux.hideCargando();
            console.log(dataDeudas);
            if(dataDeudas.status == 'ERROR') {
              this.ux.alerta(dataDeudas.error.message);
            } else if(dataDeudas.status == 'OK') {
              let objDeudas = dataDeudas.data;
              this.deudas = {
                gratuidad: objDeudas.gratuidad,
                saldoBoleta: objDeudas.saldoBoleta,
                saldoHistorico: objDeudas.saldoHistorico
              };
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });


  }

  ionViewDidLoad() {
    this.ga.trackView('page-deudas');
  }

  llamarAranceles() {
    this.ux.showCargando();
    this.callNumber.callNumber(this.numeroSeleccionado, true)
      .then(() => {
        this.ux.hideCargando();
        console.log('Realizando llamada...');
      })
      .catch(() => {
        this.ux.hideCargando();
        this.ux.alerta('Error al realizar la llamada.');
      });
  }

  portalAcademico() {
    this.nativeStorage.getItem('token')
      .then(data => {
        var token = data;
        this.ux.showCargando();
        this.portalAcademicoProvider.getUrlPortal(token)
          .subscribe(dataPortal => {
            this.ux.hideCargando();
            if(dataPortal.hasOwnProperty('data')) {
              window.open(dataPortal.data.url, '_system');
            } else {
              window.open(GlobalVar.PORTAL_ACADEMICO, '_system');
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  aranceles() {
    window.open(this.web_aranceles, '_system');
  }
}