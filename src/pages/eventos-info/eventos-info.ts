import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'eventos-info-page',
  templateUrl: 'eventos-info.html',
})
export class EventosInfoPage {
  evento: any;

  constructor(
    private ga: GoogleAnalytics,
    private navParams: NavParams
  ) {
    this.evento = this.navParams.get('evento');
    console.log('Evento: ', this.evento);
  }

  ionViewDidLoad() {
    this.ga.trackView('page-eventos-info');
  }
}