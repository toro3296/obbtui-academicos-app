import { Component } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {
  url_1: string;
  url_2: string;
  version: string;
  package: string;

  constructor(private appVersion: AppVersion) {
    this.url_1 = 'http://observatoriotui.uv.cl';
    this.url_2 = 'http://dtic.uv.cl';

    // Versión de la aplicación
    this.appVersion.getVersionNumber()
      .then(version => {
        this.version = version;
      })
      .catch(err => {
        this.version = err;
        console.log('Error al obtener la versión ', err);
      });

    // Package name de la aplicación
    this.appVersion.getPackageName()
      .then(packageName => {
        this.package = packageName;
      })
      .catch(err => {
        this.package = err;
        console.log('Error al obtener el packageName ', err);
      });
  }

  ionViewDidLoad() {

  }

  OBBTUI() {
    window.open(this.url_1, '_system');
  }

  DTIC() {
    window.open(this.url_2, '_system');
  }
}