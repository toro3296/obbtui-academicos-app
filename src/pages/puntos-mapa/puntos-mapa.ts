import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavParams, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { UxProvider } from '../../providers/ux/ux';
declare var google;

@Component({
  selector: 'page-puntos-mapa',
  templateUrl: 'puntos-mapa.html',
})
export class PuntosMapaPage {
  @ViewChild('mapContainer') mapContainer: ElementRef;
  map: any;
  punto: any;
  loading: any;

  constructor(
    private platform: Platform,
    private navParams: NavParams,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation,
    private ga: GoogleAnalytics,
    private ux: UxProvider
  ) {
    this.punto = this.navParams.get('punto');
  }

  ionViewDidLoad() {
    this.platform.ready()
      .then(() => {
        this.platform.pause
          .subscribe(() => {
            this.ux.hideCargando();
          });
      });
  }

  ionViewDidEnter() {
    this.ga.trackView('page-puntos-mapa');
  }

  ionViewWillEnter() {
    this.displayGoogleMap();
    this.addMarkerToMap(this.punto);
  }

  displayGoogleMap() {
    let latLng = new google.maps.LatLng(this.punto.latitud, this.punto.longitud);

    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapContainer.nativeElement, mapOptions);
  }

  addMarkerToMap(marker) {
    var position = new google.maps.LatLng(marker.latitud, marker.longitud);
    var puntoMarker = new google.maps.Marker({ position: position, title: marker.name });
    puntoMarker.setMap(this.map);
  }

  comoLlegar() {
    // Cargando
    this.ux.showCargando();
    this.geolocation.getCurrentPosition()
      .then(resp => {
        let options: LaunchNavigatorOptions = {
          start: [ resp.coords.latitude, resp.coords.longitude ]
        };

        this.launchNavigator.navigate([ this.punto.latitud, this.punto.longitud ], options)
          .then(success => {
            this.ux.hideCargando();
            console.log('Navegador lanzado.');
          }, err => {
            console.log('Error al lanzar el navegador', err);
            this.ux.hideCargando();
            this.ux.alerta('Tu dispositivo no puede iniciar el navegador.');
          });
      })
      .catch(error => {
        console.log('Error al obtener la posición: ', error);
        console.log(error.code + ': ' + error.message);
        this.ux.hideCargando();
        this.ux.alerta('No se pudo obtener tu posición actual. Recuerda habilitar tu GPS.');
      });
  }
}