import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NavController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { PuntosProvider } from '../../providers/puntos/puntos';
import { PuntosMapaPage } from '../puntos-mapa/puntos-mapa';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-puntos',
  templateUrl: 'puntos.html',
})
export class PuntosPage {
  puntos: any;
  puntosFiltrados: any;
  str_punto: string;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private navCtrl: NavController,
    private puntosProvider: PuntosProvider,
    private ux: UxProvider
  ) {
    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.puntosProvider.getPuntos(token)
          .subscribe(dataPuntos => {
            this.ux.hideCargando();
            console.log(dataPuntos);
            if(dataPuntos.status == 'ERROR') {
              this.ux.alerta(dataPuntos.error.message);
            } else if(dataPuntos.status == 'OK') {
              this.puntos = dataPuntos.data;
              this.puntosFiltrados = this.puntos;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-puntos');
  }

  buscarPunto(item) {
    this.puntosFiltrados = this.puntos.filter(elem => {
      return elem.nombre.toLowerCase().search(this.str_punto.toLowerCase()) != -1;
    });
  }

  abrirMapa(punto) {
    this.navCtrl.push(PuntosMapaPage, { punto: punto });
  }

  cancelarBusqueda(item) {
    this.puntosFiltrados = this.puntos;
  }
}