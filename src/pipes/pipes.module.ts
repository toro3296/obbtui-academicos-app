import { NgModule } from '@angular/core';
import { FormatoPesosPipe } from './formato-pesos/formato-pesos';
import { FormatoFechaPipe } from './formato-fecha/formato-fecha';

@NgModule({
	declarations: [
		FormatoPesosPipe,
    FormatoFechaPipe
	],
	imports: [],
	exports: [
		FormatoPesosPipe,
    FormatoFechaPipe
	]
})
export class PipesModule {}