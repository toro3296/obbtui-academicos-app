import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GlobalVar } from '../../config';

@Injectable()
export class AccesoProvider {
  public url: string;

  constructor(public http: HttpClient) {
    this.url = GlobalVar.BASE_API_URL + 'acceso/';
  }

  acceder(run: string, contrasena: string, dispositivo: string): Observable<any> {
    let arg = { run: run, contrasena: contrasena, dispositivo: dispositivo };
    return this.http
      .post(this.url, { arg: JSON.stringify(arg) })
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}




