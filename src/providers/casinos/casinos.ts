import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVar } from '../../config';

@Injectable()
export class CasinosProvider {
  url_casinos: string;
  url_menus: string;

  constructor(private http: HttpClient) {
    this.url_casinos = GlobalVar.BASE_API_URL + 'casinos';
    this.url_menus = GlobalVar.BASE_API_URL + 'casinos/menus';
  }

  getCasinos(token: string): Observable<any> {
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return this.http
      .post(this.url_casinos, null, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  getMenus(token: string, casino: number, fecha: number): Observable<any> {
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    let arg = { casino: casino, fecha_inicio: fecha, fecha_fin: fecha };
    return this.http
      .post(this.url_menus, { arg: JSON.stringify(arg) }, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}
