import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { OneSignal } from '@ionic-native/onesignal';
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GlobalVar } from '../../config';

@Injectable()
export class NotificacionesProvider {
  url: string;
  url_tipos: string;
  url_enviar: string;

  constructor(
    private http: HttpClient,
    private nativeStorage: NativeStorage,
    private oneSignal: OneSignal,
    private platform: Platform
  ) {
    this.url = GlobalVar.BASE_API_URL + 'notificaciones';
    this.url_tipos = GlobalVar.BASE_API_URL + 'notificaciones/tipos';
    this.url_enviar = GlobalVar.BASE_API_URL + 'notificaciones/enviar';
  }

  startNotificacionesPush() {
    this.platform.ready()
      .then(() => {
        if(this.platform.is('cordova')) {
          console.log('Cargando notificaciones push');
          console.log(GlobalVar.ONESIGNAL_ID, ' - ', GlobalVar.FIREBASE_PID);
          this.oneSignal.startInit(GlobalVar.ONESIGNAL_ID, GlobalVar.FIREBASE_PID);
          this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
          this.oneSignal.handleNotificationReceived()
            .subscribe(() => {
              console.log('Notificación recibida.');
            });

          this.oneSignal.handleNotificationOpened()
            .subscribe(() => {
              console.log('Notificación abierta.');
            });

          this.oneSignal.endInit();

          // Obtener identificador del dispositivo
          this.oneSignal.getIds()
            .then(data => {
              console.log('userId', data.userId);
              this.nativeStorage.setItem('dispositivo', data.userId)
                .then(() => {
                  console.log('Elemento \'dispositivo\' guardado satisfactoriamente: ', data.userId);
                }, err => {
                  console.log('Error al guardar \'dispositivo\': ', err);
                });
            })
            .catch(err => {
              console.log('Error al obtener dispositivo: ', err);
            });

        } else {
          console.log('Notificaciones push no funcionan en navegadores.');
        }
      });
  }

  getNotificaciones(token: string): Observable<any> {
    var headers = { headers: new HttpHeaders().set('Authorization', token) };
    return this.http
      .post(this.url, null, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  getTipos(token: string): Observable<any> {
    var headers = { headers: new HttpHeaders().set('Authorization', token) };
    return this.http
      .post(this.url_tipos, null, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  sendNotificacion(token: string, asignatura: object, tipo: string, mensaje: string, estudiantes: object): Observable<any> {
    var headers = { headers: new HttpHeaders().set('Authorization', token) },
      arg = { asignatura: asignatura, tipo: tipo, mensaje: mensaje, estudiantes: estudiantes };
    return this.http
      .post(this.url_enviar, { arg: JSON.stringify(arg) }, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error.', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}